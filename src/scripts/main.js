// Open/Close menu and change Hamburger icon
$(document).ready(() => {
    $(document).on('click', () => {
        if($('.header__menu').hasClass('header__menu--active')) {
            $('.header__menu').removeClass('header__menu--active');
            $('.header__hamburger-icon').removeClass('fa-times');
        }
    });

    $('.header__hamburger').on('click', (event) => {
        event.stopPropagation();
        $('.header__menu').toggleClass('header__menu--active');
        $('.header__hamburger-icon').toggleClass('fa-times');
    });
});



//Appear blocks
const appear = (container, elements) => {
    setInterval(function() {
        if ($(window).scrollTop() >= container-500) {
            let animDelay = 0;
            $(elements).each(function(){
                $(this).delay(animDelay).animate({
                    opacity:1
                },500);
                animDelay += 500;
            });
            clearInterval(appear);
        }
    }, 250);
};

$(document).ready(() => {
    const fadeAboutDiv= $(".about__description").offset().top;
    appear(fadeAboutDiv, '.fade-in');
});



//Tabs Experience block
$('[data-tab]').click(function () {
    $(this)
        .addClass('experience__skills-list-item_active')
        .siblings('[data-tab]')
        .removeClass('experience__skills-list-item_active');
    $(`[data-content = "${$(this).data('tab')}"]`)
        .addClass('experience__knowledge-list-item_active')
        .siblings('[data-content]')
        .removeClass('experience__knowledge-list-item_active');
});



// Animated scroll to label
$('a[href*=\\#]').on('click', function () {
    let elementClicked = $(this).attr('href');
    $('html, body').stop().animate({
        scrollTop: $(elementClicked).offset().top
    },2000);
});

$('.btn_page-up').on('click', function () {
    $('html, body').stop().animate({
        scrollTop: 0
    },2000);
});



// Show and hide Button "Page up"
$(window).on('scroll', function () {
    if($(this).scrollTop() > $(this).height()) {
        $('.btn_page-up').show();
    } else {
        $('.btn_page-up').hide();
    }
});

